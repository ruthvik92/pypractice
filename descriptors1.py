class MyDescriptor(object):
    def __init__(self):
        self.__age = {}

    def __get__(self, instance, owner):
        return self.__age[instance]

    def __set__(self, instance, value):
        if not isinstance(value, int):
            raise TypeError("age must be Integer only")

        if value <0 or value > 122:
            raise ValueError("0 < Age < 122!!!")

        self.__age[instance] = value

    def __delete__(self, instance):
        del self.__age[instance]




class Person(object):
    age = MyDescriptor()

    def __init__(self, name, age):
        self.name = name
        self.age = age


    def __str__(self):
        return "{0} is {1} years old!".format(self.name, self.age)


obj1 = Person("sam", 20)
print(str(obj1))

obj2 = Person("john", 65)
print str(obj2)

## instance = obj1, owner = Person
