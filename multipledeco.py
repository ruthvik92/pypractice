import pdb
pdb.set_trace()
def star(func):    
    def inner(*args, **kwargs):       
        print("*" * 30)
        func(*args, **kwargs)
        print("*" * 30)
    return inner

def percent(func):    
    def inner(*args, **kwargs):       
        print("%" * 30)
        func(*args, **kwargs)
        print("%" * 30)
    return inner

#@star
#@percent
def printer(msg):
    print(msg)
printer = star(percent(printer))
printer("Hello")


#printer = star(percent(printer))
