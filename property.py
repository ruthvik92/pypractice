'''class Employee:


    def __init__(self, first, last):
        self.first = first
        self.last = last
        self.email = self.first+'.'+self.last+'@email.com'

    def fullname(self):
        return '{} {}'.format(self.first,self.last)


emp1 = Employee('John', 'Smith')

print(emp1.first)
print(emp1.email)
print(emp1.fullname())

emp1.first = 'Jim'
print(emp1.first)
print(emp1.email)
print(emp1.fullname())'''
'''
class Employee:


    def __init__(self, first, last):
        self.first = first
        self.last = last

    def email(self):
        return '{}.{}@email.com'.format(self.first,self.last)

    def fullname(self):
        return '{} {}'.format(self.first,self.last)


emp1 = Employee('John', 'Smith')

print(emp1.first)
print(emp1.email())
print(emp1.fullname())

emp1.first = 'Jim'
print(emp1.first)
print(emp1.email())
print(emp1.fullname())'''

class Employee(object):


    def __init__(self, first, last):
        self.first = first
        self.last = last
    @property
    def email(self):
        return '{}.{}@email.com'.format(self.first,self.last)

    @property
    def fullname(self):
        return '{} {}'.format(self.first,self.last)

    @fullname.setter
    def fullname(self,name):
        first, last = name.split(' ')
        self.first = first
        self.last = last

    @fullname.deleter
    def fullname(self):
        print "Delete fullname"
        self.first = None
        self.last = None 


emp1 = Employee('John', 'Smith')
print(emp1.first)
print(emp1.email)
print(emp1.fullname)


emp1.fullname = 'Ruthvik Vaila'
#emp1.first = 'Jim'
print(emp1.first)
print(emp1.email)
print(emp1.fullname)

del emp1.fullname




