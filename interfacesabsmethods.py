
from abc import ABCMeta, abstractmethod
# Interface

class Shape:
    __metaclass__ = ABCMeta
    
    @abstractmethod
    def area(self):
        pass

    @abstractmethod
    def perimeter(self):
        pass

class Rectangle(Shape):
    def __init__(self, width, height):

        self.width = width
        self.height = height
        #super(Rectangle, self).__init__()
        #Shape.__init__(self)
    def area(self):
        print "calling area here"
        return self.width*self.height

    def perimeter(self):
        return self.width*2 + self.height*2


class Square(Rectangle):
    def __init__(self, side):
        self.side = side
        super(Square, self).__init__(side, side)
        

    '''def area(self):
        print "calling area here"
        return self.side*self.side'''


rect = Rectangle(5,6)
print rect.area()

sqr = Square(5)
print sqr.area()
