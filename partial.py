from functools import partial
 def adder(a,b,c):
	print('a:{},b:{},c:{}'.format(a,b,c))
	ans = a+b+c
	print(ans)

partial_adder = partial(adder,1,2) ## now partial_adder is a callable that can take only one argument
partial_adder(3)
#a:1,b:2,c:3
#6
### notice that in the above example a new callable was returned that will take parameter (c) as
### it's argument. Note that it is also the last argument to the function.


args = [1,2]
partial_adder = partial(adder,*args)
partial_adder(3)
#a:1,b:2,c:3
#6
### notice that * was used to unpack the non-keyword arguments and the callable returned in terms of 
### arguments it can take is same as above


### below example demonstrates that partial returns a callable which will take the 
### undeclared parameter (a) as an argument
def adder(a,b=1,c=2,d=3,e=4):
	print('a:{},b:{},c:{},d:{},e:{}'.format(a,b,c,d,e))
	ans = a+b+c+d+e
	print(ans)

	
partial_adder = partial(adder,b=10,c=2)
partial_adder(20)
#a:20,b:10,c:2,d:3,e:4
#39
kwargs = {'b':10,'c':2}
partial_adder = partial(adder,**kwargs)
partial_adder(20)
#a:20,b:10,c:2,d:3,e:4
#39
################passing extra keyword arguments (unused) to a function ##########
def adder(a,b=1,c=2,d=3,e=4,**kwargs):
	print('a:{},b:{},c:{},d:{},e:{}'.format(a,b,c,d,e))
	ans = a+b+c+d+e
	print(ans)
	print(kwargs)
kwargs = {'b':10,'c':2, 'g':10}
adder(1,**kwargs)
#a:1,b:10,c:2,d:3,e:4
#20
#{'g': 10}
kwargs = {'b':10,'c':2}
adder(1,**kwargs)
#a:1,b:10,c:2,d:3,e:4
#20
#{}
################### partial with unused keyword argument ###################
kwargs = {'b':10,'c':2, 'g':10}
partial_adder = partial(adder,**kwargs)
partial_adder(1)
#a:1,b:10,c:2,d:3,e:4
#20
#{'g': 10}
