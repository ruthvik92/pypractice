# https://stackoverflow.com/questions/45312657/understanding-borg-singleton-pattern-in-python
# https://stackoverflow.com/questions/34568923/borg-design-pattern
import sys

shared_dict = {}


class A(object):
    def __init__(self):
        self.__dict__ = shared_dict


if __name__ == "__main__":
    objs = [A() for i in range(10)]
    objs[0].x = 123  # <-- only first instance is modified

    for obj in objs:
        print(obj.x)  # <--- but the change can be seen in rest of the 9 instances!

    print(shared_dict)  # <-- let's see what's stored in the global fictionary!

sys.exit()

### Try setting __dict__ to a list!!! See what happens!
shared_dict = []


class A(object):
    def __init__(self):
        self.__dict__ = shared_dict


if __name__ == "__main__":
    objs = [A() for i in range(10)]

## TRIGGERS-->TypeError: __dict__ must be set to a dictionary, not a 'list'
## See the below C code for why!

# int
# PyObject_GenericSetDict(PyObject *obj, PyObject *value, void *context)
# {
#    PyObject **dictptr = _PyObject_GetDictPtr(obj);
#    ...
#    if (!PyDict_Check(value)) {
#        PyErr_Format(PyExc_TypeError,
#                     "__dict__ must be set to a dictionary, "
#                     "not a '%.200s'", Py_TYPE(value)->tp_name);
#        return -1;
#    }
#    Py_INCREF(value);
#    Py_XSETREF(*dictptr, value);  # Set the dict to point to new dict
#    return 0;
# }
