from typing import Dict

# https://stackoverflow.com/questions/45312657/understanding-borg-singleton-pattern-in-python
# https://www.geeksforgeeks.org/singleton-pattern-in-python-a-complete-guide/
# https://www.oreilly.com/library/view/python-cookbook/0596001673/ch05s23.html
# https://stackoverflow.com/questions/34568923/borg-design-pattern


class Borg:
    _shared_state: Dict[str, str] = {}

    def __init__(self) -> None:
        self.__dict__ = self._shared_state
        # self.__dict__ = {}
        print("id {} shared state(self.__dict__) {}".format(id(self), self.__dict__))
        print(
            "id {} shared state(cls._shared_state) {}".format(
                id(self), self._shared_state
            )
        )


class MyBorg(Borg):
    def __init__(self, state: str = None) -> None:
        super().__init__()
        if state:
            self.state = state
        else:
            if not hasattr(self, "state"):
                print("id:{} was here..".format(id(self)))
                self.state = "init"

    def __str__(self) -> str:
        return self.state


if __name__ == "__main__":
    rm1 = MyBorg()
    print("rm1 id is:{}".format(id(rm1)))
    print(rm1.__dict__)
    # print(rm1._shared_state)
    rm2 = MyBorg()
    print("rm2 id is:{}".format(id(rm2)))
    print(rm2.__dict__)
    # print(rm2._shared_state)
    rm1.state = "Idle"
    rm2.state = "Running"
    print("rm1:{}".format(rm1))
    print("rm2:{}".format(rm2))
