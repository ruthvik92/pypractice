class SingletonClass(object):
    global_dict = {}
    # https://www.geeksforgeeks.org/singleton-pattern-in-python-a-complete-guide/
    def __new__(cls):
        print(cls)
        if not hasattr(cls, "instance"):
            print('Checking hasattr(cls, "instance")')
            print("super(SingletonClass, cls) {}".format(super(SingletonClass, cls)))
            cls.instance = super(SingletonClass, cls).__new__(cls)
        return cls.instance


# if __name__ == "__main__":
singletonclass = SingletonClass()
print(SingletonClass.__dict__)
print(singletonclass.__dict__)
new_singletonclass = SingletonClass()

print(singletonclass is new_singletonclass)
singletonclass.variable_x = "var_x"
print(new_singletonclass.variable_x)

################################################################################
class RegularClass(object):
    def __init__(self):
        pass


regular_class = RegularClass()
new_regular_calss = RegularClass()

print(regular_class is new_regular_calss)
regular_class.variable_x = "var_x"
# print(
#    new_regular_calss.variable_x
# )  # AttributeError: 'RegularClass' object has no attribute 'variable_x'


################################################################################
class SingletonChild(SingletonClass):
    pass


singleton_parent = SingletonClass()
singleton_child = SingletonChild()

print(singleton_parent is singleton_child)
print(singleton_child.variable_x)
