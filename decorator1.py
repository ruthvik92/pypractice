'''def outer_function(msg):
    def inner_function():
        print msg

    return inner_function



hi_func = outer_function('Hi')

bye_func = outer_function('bye')'''

'''def decorator_function(original_function):
    def wrapper_function():
        print ("wrapper executed  before {}".format(original_function.func_name))
        return original_function()
    return wrapper_function

@decorator_function
def display():
    print "now display function executed"


display()

#decorated_display = decorator_function(display)

#decorated_display()'''

'''def decorator_function(original_function):
    def wrapper_function(*args, **kwargs):
        print ("wrapper executed  before {}".format(original_function.func_name))
        return original_function(*args, **kwargs)

    return wrapper_function


@decorator_function
def display_info(name, age):
    print "dsiplay_info ran with arguments ({}, {})".format(name, age)


#decorated_display = decorator_function(display_info)
display_info('john', 25)'''

class decorator_class(object):

    def __init__(self, original_function):
        self.original_function = original_function



    def __call__(self, *args, **kwargs):
        print ("call executed  before {}".format(self.original_function.func_name))
        return self.original_function(*args, **kwargs)
        

@decorator_class
def display_info(name, age):
    print "dsiplay_info ran with arguments ({}, {})".format(name, age)   

display_info('john',12)

'''def my_logger(orig_func):
    import logging
    logging.basicConfig(filename='{}.log'.format(orig_func.func_name),level=logging.INFO)

    def wrapper(*args, **kwargs):
        logging.info('Ran with args:{}, and kwargs:{}'.format(args,kwargs))
        return orig_func(*args,**kwargs)
    return wrapper




@my_logger
def display_info(name,age):
    print "display_info ran with arguments ({},{})".format(name,age)

display_info('John', 25)'''

"""def my_timer(orig_func):
    import time

    def wrapper(*args, **kwargs):
        t1 = time.time()
        result = orig_func(*args, **kwargs)
        t2 = time.time()-t1
        print "{} ran in: {} sec".format(orig_func.__name__,t2)
    return wrapper

def display_info(name,age):
    print "display_info ran with arguments ({},{})".format(name,age)


un_executed = my_timer(display_info)
un_executed('john',25)"""




