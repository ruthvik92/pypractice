import yaml


class Employee(object):
    def __init__(self, name, id):
        self._name, self._id = name, id


list_of_employees = [Employee("name1", 1), Employee("name2", 2), Employee("name3", 3)]


def employee_representer(
    dumper: yaml.SafeDumper, emp: Employee
) -> yaml.nodes.MappingNode:
    """Represent Employee instance as a YAML mapping node"""

    return dumper.represent_mapping("!Employee", {"name": emp._name, "id": emp._id})


def get_dumper():
    """Add a representer to a YAML dumper"""
    safe_dumper = yaml.SafeDumper
    safe_dumper.add_representer(Employee, employee_representer)


with open("representer.yml", "w") as stream:
    stream.write(yaml.dump(list_of_employees, get_dumper()))
