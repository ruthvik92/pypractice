import yaml

# https://matthewpburruss.com/post/yaml/
# At the core of using PyYAML is the concept of constructors, representers, and tags.
#
# From a high-level, a constructor allows you to take a YAML node and return a class
# instance; a representer allows you to serialize a class instance into a YAML node; and a
# tag helps PyYaml know which constructor or representer to call! A tag uses the special
# character ! preceding the tag name to label a YAML node.
#
# Let’s take a look at an example YAML file that does not have any tags.


class Employee(object):
    def __init__(self, name, id):
        self._name, self._id = name, id


def employee_constructor(
    loader: yaml.SafeLoader, node: yaml.nodes.MappingNode
) -> Employee:
    """Construct an Employee"""

    return Employee(**loader.construct_mapping(node))


def get_loader():
    """Add constructors to PyYAML loader"""
    loader = yaml.SafeLoader
    loader.add_constructor("!Employee", employee_constructor)
    return loader


with open("example.yml", "rb") as file:
    data = yaml.load(file, Loader=get_loader())

print(data)
# yaml.load(open("example.yml", "rb"), Loader=get_loader())
