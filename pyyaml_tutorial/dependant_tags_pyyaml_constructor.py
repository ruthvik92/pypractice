import yaml


class MyClass(object):
    def __init__(self, param1, order):
        print(self)
        print("Initializing with name:{} and order:{}".format(param1, order))
        print("\n")


def my_class_loader(loader, node):
    """A loader for functions"""

    return MyClass(**loader.construct_mapping(node))


def get_loader():
    """Return a yaml loader"""
    loader = yaml.SafeLoader
    loader.add_constructor("!MyClass", my_class_loader)
    return loader


yaml.load(open("deep_config.yml", "rb"), Loader=get_loader())
