import json
from my_scraper import handle_imdb, handle_goodreads

# https://matthewpburruss.com/post/yaml/


for scrape_config in json.load(open("harry_potter_config.json", "rb")):
    if scrape_config["provider"] == "imdb":
        handle_imdb(scrape_config["url"])
    elif scrape_config["provider"] == "goodreads":
        handle_goodreads(scrape_config["url"])
    else:
        raise ValueError(f"Unknown provider {scrape_config['provider']}")
