# One can use this or use Yacs YAML config package and then use
# Directory pattern as in
# https://github.com/mrlooi/rotated_maskrcnn/blob/master/maskrcnn_benchmark/utils/registry.py
import yaml


def func_loader(loader, node):
    """A loader for functions"""
    params = loader.construct_mapping(node)  # get node mappings
    module = __import__(
        params["module"], fromlist=[params["name"]]
    )  # laod python module

    return getattr(module, params["name"])  # get function from module


def get_loader():
    "Return a Yaml loader"
    loader = yaml.SafeLoader
    loader.add_constructor("!Func", func_loader)
    return loader


config = yaml.load(open("harry_potter_config.yml", "rb"), Loader=get_loader())

for scrape_config in config:
    scrape_config["handler"](scrape_config["url"])
