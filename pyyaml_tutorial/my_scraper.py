import sys
import os


def handle_imdb(link: str) -> None:
    print("Handling IMDB from the link:{}".format(link))


def handle_goodreads(link: str) -> None:
    print("Handling goodreads from the link:{}".format(link))
