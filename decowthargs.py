def smart_divide(func):
    def inner(a,b):
        print "I am going to divide", a, b
        if b==0:
            print " whoops! can't divide"
            return

        return func(a,b)
    return inner
    
@smart_divide
def divide(a,b):
    return a/b


print divide(2,1)
