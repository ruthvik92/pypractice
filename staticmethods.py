class Board:
    _game_tiles=[]

    def __init__(self, length, width):

        if not Board._game_tiles:
            for _ in xrange(length*width):
                Board._game_tiles.append(Tile())




    @staticmethod
    def move_together(x_amount, y_amount):

        for tile in Board._game_tiles:
            tile.move(x_amount, y_amount)




    @staticmethod
    def print_locations():
        for tile in Board._game_tiles:
            print tile.x, tile.y





class Tile:

    def __init__(self):
        self.x = 0
        self.y = 0



    def move(self, x_amount, y_amount):
        self.x+=x_amount
        self.y += y_amount


my_board = Board(4,4)
Board.print_locations()
Board.move_together(10,10)
Board.print_locations()

print my_board._game_tiles
