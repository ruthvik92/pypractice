import numpy
from mpi4py import MPI
comm = MPI.COMM_WORLD
rank = comm.Get_rank()
a = numpy.zeros([1,3],dtype='i')

if rank ==0:
    a = numpy.asarray([1,2,3],dtype='i')
    comm.Send(a, dest=1)
    print('Process {} set a to {} and sent it').format(rank, a)

if rank ==1:
    print('Value of a {} in process {} Before receiving').format(a,rank)
    comm.Recv(a,source=0)
    print('Value of a {} in process {} after receiving').format(a,rank)
