
from mpi4py import MPI
import numpy
import sys

comm=MPI.COMM_WORLD
rank = comm.Get_rank()
size = comm.Get_size()


n= int(sys.argv[1])


x = numpy.linspace(0,100,n) if comm.rank ==0 else None
y = numpy.linspace(20,300,n) if comm.rank ==0 else None


dot = numpy.array([0],dtype='float')
local_n = numpy.array([0])

if rank ==0:
    if(n!=y.size):
        print 'vector length mismathc'
        comm.Abort()


    if(n%size !=0):
        print 'the number of processors must be evenly divide by n'
        comm.Abort()
    
    local_n = numpy.array([n/size])



comm.Bcast(local_n, root=0)

local_x = numpy.zeros(local_n)
local_y = numpy.zeros(local_n)

comm.Scatter(x,local_x,root=0)
comm.Scatter(y,local_y,root=0)

local_dot = numpy.array([numpy.dot(local_x,local_y)])

comm.Reduce(local_dot, dot, op=MPI.SUM)

if rank==0:
    print('dot product is {} computed in parallel').format(dot[0])
    print ('dot product is {} computed in serial').format(numpy.dot(x,y))
