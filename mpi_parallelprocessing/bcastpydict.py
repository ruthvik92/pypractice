import numpy as np
from mpi4py import MPI

comm = MPI.COMM_WORLD
rank = comm.Get_rank()

if rank == 0:
    #data = {'key1' : [7, 2.72, 2+3j],
    #        'key2' : ( 'abc', 'xyz')}
    #data =[1,2]
    data = np.arange(10) #smaller bcast can send any kind of python objects just like send/recv.
else:
    data = None
    pass
data = comm.bcast(data, root=0)
print('process {} has data {}').format(rank,data)
