from mpi4py import MPI
import numpy as np
comm = MPI.COMM_WORLD
rank = comm.Get_rank()
data = {}
if rank==0:
    #data=np.arange(10,dtype='i')
    data = {'a':7,'b':3}
    comm.send(data, dest=1, tag=11)
    print 'process 0 sent', data
elif rank==1:
    #comm.recv(data,source=0, tag=11) #this doesnt work here cos, in smaller case recv/send received object is the
    #return value.
    data = comm.recv(source=0, tag=11)
    print 'process 1 received',data
