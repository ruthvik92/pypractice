import numpy
import sys
from mpi4py import MPI
comm = MPI.COMM_WORLD
rank = comm.Get_rank()
size = comm.Get_size()

randNum = numpy.zeros(1)
root =0

if rank==0:
    randNum = numpy.random.random_sample(1)
    comm.Send(randNum,dest=rank+1)
    print('Process {} sent the number {} to process {}').format(rank, randNum[0], rank+1)
    comm.Recv(randNum,source=size-1)
    print('Process {} received the number {} from process {}').format(rank, randNum[0],size-1)
    


elif rank==size-1:
    randNum = numpy.random.random_sample(1)
    comm.Send(randNum, dest=root)
    print('Process {} sent the number {} to process {}').format(rank, randNum[0], root)
    comm.Recv(randNum,source=rank-1)
    print('Process {} received the number {} from process {}').format(rank, randNum[0],rank-1)
elif rank!=0 or rank!=size-1:
    randNum = numpy.random.random_sample(1)
    comm.Send(randNum,dest = rank+1)
    print('Process {} sent the number {} to process {}').format(rank, randNum[0], rank+1)
    comm.Recv(randNum,source=rank-1)
    print('Process {} received the number {} from process {}').format(rank, randNum[0],rank-1)

