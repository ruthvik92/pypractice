
from mpi4py import MPI

comm = MPI.COMM_WORLD
size = comm.Get_size()
rank = comm.Get_rank()

data = [rank for i in range(0,size)]
print('process {} had data {}').format(rank,data)
comm.Barrier()
data = comm.gather(data, root=0)
if rank == 0:
    print('length of the data in process {} is increased to {}').format(rank,len(data))
    for i in range(size):
        assert data[i] == [rank+i for j in range(0,size)]
else:
    #print 'data in other processes:',data
    assert data is None

print('process {} has data {}').format(rank,data)
