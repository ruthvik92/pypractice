from mpi4py import MPI
Comm = MPI.COMM_WORLD
rank = Comm.Get_rank()
size = Comm.Get_size()
root = 0
if(rank==root):
    if(size!=5):
        print 'Error: This program must run with 5 processes'
        Comm.Abort()
    elif size==5:
        print 'Success!'
        Comm.Abort()
        
