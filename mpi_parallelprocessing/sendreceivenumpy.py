from mpi4py import MPI
import numpy as np

comm=MPI.COMM_WORLD
rank = comm.Get_rank()

if rank==0:
    data = np.arange(10,dtype='i')
    comm.Send([data, MPI.INT],dest=1,tag=77)
    print 'data sent is',data
elif rank==1:
    data = np.empty(10,dtype='i')
    #data = comm.Recv(source=0, tag=77)#this doesnt work cos, big Recv/Send use buffers to receive data.
    comm.Recv(data,source=0, tag=77)
    print 'data received is',data
