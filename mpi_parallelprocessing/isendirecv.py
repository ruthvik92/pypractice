from mpi4py import MPI
comm=MPI.COMM_WORLD
rank = comm.Get_rank()
data ={}
if rank==0:
    data = {'a':7,'b':3}
    req = comm.isend(data,dest=1, tag=11)
    print 'process 0 sent:',data
    data =2
    req = comm.isend(data,dest=1, tag=11)
    #req.wait()
    print "finished sending from process 0"

elif rank==1:
    req = comm.irecv(source=0, tag=11)
    print 'reached here from process 1(prior to wait)'
    data = req.wait()
    req = comm.irecv(source=0, tag=11)
    data1 = req.wait()
    print 'reached here from process 1(after wait)'
    print 'process 1 received:',data
    print 'process 1 received:',data1
