import numpy as np
from mpi4py import MPI
comm = MPI.COMM_WORLD
rank = comm.Get_rank()
size = comm.Get_size()
Length = 3

if rank ==0:
    x = np.linspace(1,size*Length,size*Length)


else:
    x = None



x_local = np.zeros(Length)

comm.Scatter(x,x_local, root=0)

print('process {} x {}').format(rank,x)
print('process {} x_local {}').format(rank,x_local)

