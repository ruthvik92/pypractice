import numpy
import sys

a = float(sys.argv[1])
b = float(sys.argv[2])
n = int(sys.argv[3])

def f(x):
    return x*x


def integrateRange(a,b,n):
    integral = -(f(a) + f(b))/2.0
    for x in numpy.linspace(a,b,n+1):
        integral = integral + f(x)
    integral = integral*(b-a)/n
    return integral


integral = integrateRange(a,b,n)
print('with n:{} trapezoids, our estimate of the integral from {} to {} is {}').format(n,a,b,integral)
