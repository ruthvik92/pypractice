from mpi4py import MPI
comm = MPI.COMM_WORLD
rank = comm.Get_rank()

a = 6
b =3

if rank==0:
    print a+b, rank
elif rank==1:
    print a*b, rank
else:
    print a/b, rank
