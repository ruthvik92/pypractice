import numpy as np
from mpi4py import MPI
from mpi4py.MPI import ANY_SOURCE
comm = MPI.COMM_WORLD
rank = comm.Get_rank()
size = comm.Get_size()
start = MPI.Wtime()
rand_num=np.empty(5,dtype='float')
bands  = [(0, 65), (66, 132), (133, 198), (199, 264), (265, 330), (331, 396), (397, 462), (463, 529)]
volts = {'map1':[478], 'map2':[346], 'map3':[213], 'map4':[81], 'map5':[34]}

for items in volts.keys():
    
    #print items, comm.rank
    if(len(volts[items])!=0):
        neur_id = volts[items][0]
	if neur_id in range(bands[comm.rank][0],bands[comm.rank][1]):
            #print 'inside if condition',items, comm.rank
	    rand_num = np.array([comm.rank for i in range(0,5)],dtype='float')
            all_ranks = [i for i in range(0,size)]
	    #print all_ranks
	    all_ranks.remove(rank)
            rest_ranks = all_ranks 
            for ranks in rest_ranks: 
	        comm.Send(rand_num, dest= ranks)
	    #comm.Bcast(rand_num, root=comm.rank)
	    #print 'broadcasted from rank', comm.rank
	
	#comm.Barrier()
	else:
	    comm.Recv(rand_num, source = ANY_SOURCE)
        comm.Barrier()  #I was hoping to stop other processes from going ahead(synchronizing)
    #print 'data and rank',rand_num,comm.rank, items
end = MPI.Wtime()
print end - start, rank

#if rank==0:
    #rand_num = np.array([1,2,3,4,5], dtype='float')
#else:
    #rand_num=np.empty(5,dtype='float')


#comm.Bcast(rand_num, root=0)
#print('process {} has the number {}').format(rank, rand_num)
