import numpy
import sys
from mpi4py import MPI
comm = MPI.COMM_WORLD
rank = comm.Get_rank()

n = int(sys.argv[1])
randNum = numpy.zeros([1,n])

if rank ==1:
    randNum = numpy.random.rand(n)
    comm.Send(randNum,dest=0, tag=11) 
    print('Process {} drew the numbers{}').format(rank, randNum)

if rank ==0:
    print('Process {} before receiving has the numbers {}').format(rank,randNum[0])
    comm.Recv(randNum, tag=11)
    print('Process {} received the number {}').format(rank, randNum[0])
