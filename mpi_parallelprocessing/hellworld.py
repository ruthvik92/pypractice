
from mpi4py import MPI
Comm = MPI.COMM_WORLD
rank = Comm.Get_rank()
size = Comm.Get_size()
print ('Hello world from process {} out of {}').format(rank,size)
