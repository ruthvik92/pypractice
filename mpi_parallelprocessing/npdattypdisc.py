from mpi4py import MPI
import numpy as np

comm=MPI.COMM_WORLD
rank=comm.Get_rank()

if rank==0:
    data= np.arange(10,dtype='i')
    comm.Send(data, dest=1,tag=13)
    print 'data sent is',data

elif rank ==1:
    data = np.empty(10,dtype='i')
    comm.Recv(data,source=0, tag=13)
    print 'data sent is',data
