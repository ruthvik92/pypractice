import numpy
import sys
from mpi4py import MPI
from mpi4py import MPI 
from mpi4py.MPI import ANY_SOURCE

comm = MPI.COMM_WORLD
rank = comm.Get_rank()
size = comm.Get_size()

a = float(sys.argv[1])
b = float(sys.argv[2])
n = int(sys.argv[3])


def f(x):
    return x*x


def integrateRange(a,b,n):
    integral = -(f(a)+f(b))/2.0
    for x in numpy.linspace(a,b,n+1):
        integral = integral +f(x)
    integral = integral*(b-a)/n
    return integral


h = (b-a)/n

local_n = n/size
print local_n
local_a = a+rank*local_n*h
local_b = local_a + local_n*h

integral = numpy.zeros(1) #buffer for sending values.
recv_buffer = numpy.zeros(1) #buffer for receiving values.

integral[0] = integrateRange(local_a, local_b, local_n)

if(rank ==0):
    total = integral[0] #integral value in the root node.
    for i in range(1,size):
        comm.Recv(recv_buffer, ANY_SOURCE)
        total += recv_buffer[0] #accumulating value from other nodes.

else:
    comm.Send(integral, dest = 0) #other nodes sending values.


if comm.rank ==0:
    print('with n:{} trapezoids, our estimate of integral from {} to {} is total {}').format(n,a,b,total)
