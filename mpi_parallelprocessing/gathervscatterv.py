import numpy as np
from mpi4py import MPI
comm=MPI.COMM_WORLD
rank = comm.Get_rank()

if rank ==0:
    x = np.linspace(0,100,11,dtype='float')

else:
    x = None

if rank==2:
    xlocal = np.zeros(9,dtype='float')

else:
    xlocal = np.zeros(1,dtype='float')

if rank ==0:
    print 'scatter'

comm.Scatterv([x,(1,1,9),(0,1,2),MPI.DOUBLE],xlocal,root=0)
print('process {} has {}').format(rank, xlocal)

comm.Barrier()

if rank ==0:
    print 'Gather'
    xGathered = np.zeros(11)

else:
    xGathered = None

comm.Gatherv(xlocal,[xGathered, (1,1,9),(0,1,2),MPI.DOUBLE],root=0)
print('process {} has {}').format(rank, xGathered)
