
from mpi4py import MPI
Comm = MPI.COMM_WORLD
rank = Comm.Get_rank()
size = Comm.Get_size()
if(rank%2 ==0):
    print('Hello world from process {}').format(rank)
else:
    print('Goodbye from process {}').format(rank)
