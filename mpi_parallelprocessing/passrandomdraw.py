import numpy
from mpi4py import MPI
comm = MPI.COMM_WORLD
rank = comm.Get_rank()

randNum = numpy.zeros(1)

if rank ==1:
    randNum = numpy.random.random_sample(1)
    comm.Send(randNum, dest=0) 
    print('Process {} drew the number{}').format(rank, randNum[0])

if rank ==0:
    print('Process {} before receiving has the number {}').format(rank,randNum[0])
    comm.Recv(randNum, source=1)
    print('Process {} received the number {}').format(rank, randNum[0])
