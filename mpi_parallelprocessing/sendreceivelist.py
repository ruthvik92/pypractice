import numpy
from mpi4py import MPI
comm = MPI.COMM_WORLD
rank = comm.Get_rank()
data = [0,0,0]

if rank ==0:
    a =[1,2,3]
    comm.send(a, dest=1)
    print('Process {} set a to {} and sent it').format(rank, a)

if rank ==1:
    print('Value of a {} in process {} Before receiving').format(data,rank)
    a = comm.recv(source=0)
    print('Value of a {} in process {} after receiving').format(a,rank)
