import multiprocessing as mp
import time, random, os, logging, sys
import pool_tools as ptools 
#logger = mp.log_to_stderr(logging.INFO)



def manager(n_processes, n_jobs, chunksize):
    numbersToSquare = [x for x in range(0,n_jobs)]     #I'm 
    numbersToSquare = list(zip(numbersToSquare,numbersToSquare))
    results = [] #Where the results will be stored
    pool = mp.Pool(processes=(n_processes)) 
    r = pool.map_async(ptools.worker2, numbersToSquare, callback=results.append,chunksize=chunksize)  #Using fxn "calculate", feed taskList, and values stored in "results" list
    r.wait()                # Wait on the results from the map_async
    ptools.print_pool_results(results[0],n_processes)
    return results


#Main Process
if __name__ == '__main__':
    print_results = not False
    n_processes = 4
    n_jobs = 20
    chunksize = 5
    t1 = time.time()
    results = manager(n_processes, n_jobs, chunksize)
    print("{:>25s} {:12.4f}".format("Wall clock time (s)",time.time()-t1))
    if(print_results):
        final_results = results[0]    #All of the entries only exist in the first offset
        print([items[-1] for items in final_results])      

        
