from pool_tools import *
import time
def test2(data,np):
    pool = Pool(processes=np)              
    
    # This is non-blocking
    async_results = pool.map_async(func=worker,iterable=data)
    
    # This call blocks until all results are available
    results = async_results.get()   
    
    print_pool_results(results,np)
    
np = 4
njobs = 20
            
print("Launching {} jobs on {} cores".format(njobs,np))
    
random.seed(1234)

sleep_times = [5*random.random() for i in range(njobs)]
data = zip(range(njobs),sleep_times)    # Create list of tuples (p,t)

t1 = time.time()    
test2(data,np)

print("{:>25s} {:12.4f}".format("Wall clock time (s)",time.time()-t1))
