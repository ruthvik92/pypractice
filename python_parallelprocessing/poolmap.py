import pool_tools as ptools
import multiprocessing as mp
import time, random, os, logging, sys
#logger = mp.log_to_stderr(logging.INFO)

def manager(data,np):
    pool = mp.Pool(processes=np)              

    # This function blocks until results are available
    results = pool.map(func=ptools.worker,iterable=data)
    ptools.print_pool_results(results,np)
    
    


if __name__ == "__main__":
    np = 4
    njobs = 10         
    sleep_times = [5*random.random() for i in range(njobs)]
    data = zip(range(njobs),sleep_times)    # Create list of tuples (p,t)
    random.seed(1234)
    print("Launching {} jobs on {} cores".format(njobs,np))
    t1 = time.time()
    manager(data,np)
    print("{:>25s} {:12.4f}".format("Wall clock time (s)",time.time()-t1))
