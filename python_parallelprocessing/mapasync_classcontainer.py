import multiprocessing
import time, random, os, logging, sys
#logger = mp.log_to_stderr(logging.INFO)

class NumberHolder():
    def __init__(self,numValue):
        self.numValue = numValue    #Only one attribute

def calculate(obj):
    if random.random() >= 0.5:
        startTime = time.time()
        timeWaster = [random.random() for x in range(5000000)] #Waste time.
        endTime = time.time()           #Establish end time
        print("%d object got stuck in here for %f seconds"%(obj.numValue,endTime-startTime))
    obj.numValue = obj.numValue**2
    return obj

n_processes = 4
n_jobs = 20
#Main Process
if __name__ == '__main__':
    numbersToSquare = [x for x in range(0,n_jobs)]     #I'm 
    taskList = []

    for eachNumber in numbersToSquare:
        taskList.append(NumberHolder(eachNumber))   #Create a list of objects whose numValue is equal to the numbers we want to square

    results = [] #Where the results will be stored
    pool = multiprocessing.Pool(processes=(n_processes)) #Don't use all my processing power.
    r = pool.map_async(calculate, taskList, callback=results.append)  #Using fxn "calculate", feed taskList, and values stored in "results" list
    r.wait()                # Wait on the results from the map_async

    results = results[0]    #All of the entries only exist in the first offset
    for eachObject in results:      #Loop through them and show them
        print(eachObject.numValue)      
