import multiprocessing as mp
import time, random, os, logging, sys
import pool_tools as ptools
#logger = mp.log_to_stderr(logging.INFO)



def manager(n_processes, n_jobs):
    numbersToSquare = [x for x in range(0,n_jobs)]     #I'm 
    
    results = [] #Where the results will be stored
    pool = mp.Pool(processes=(n_processes)) #Don't use all my processing power.
    r = pool.map_async(ptools.worker1, numbersToSquare, callback=results.append)  #Using fxn "calculate", feed taskList, and values stored in "results" list
    #print(os.getpid())
    r.wait()                # Wait on the results from the map_async
    return results

n_processes = 4
n_jobs = 20
#Main Process
if __name__ == '__main__':
    results = manager(n_processes,n_jobs)
    results = results[0]    #All of the entries only exist in the first offset
    for eachResult in results:      #Loop through them and show them
        print(eachResult)      
