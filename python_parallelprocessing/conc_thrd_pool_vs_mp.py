import multiprocessing as mp
import os, time

powers = list(range(20))
#def worker_process(i):
#    time.sleep(1)
#    print("PID:{}".format(os.getpid()))
#    return i * i # square the argument

def pow2(x, y):
    print("PID:{}".format(os.getpid()))
    return x**y

def process_result(return_value):
    print("Result:{}".format(return_value))

def main():
    pool = mp.Pool()
    for i in range(10):
        pool.apply_async(pow2, args=(2, powers[i],), callback=process_result)
    pool.close()
    pool.join()

if __name__ == '__main__':
    main()

print('Notice that when using multiprocessing there may be different PIDs')

print()

from concurrent.futures import ThreadPoolExecutor

with ThreadPoolExecutor(max_workers=3) as executor:
    futures = [executor.submit(pow2, 2, power) for power in powers]
    print([future.result() for future in futures])


print('Notice that when using ThreadPoolExecutor all the PIDs are same')
