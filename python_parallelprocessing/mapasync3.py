#from multiprocessing import Pool
import time, os
from pool_tools import *
final_results = []
def f(z):
    jobnum, x = z
    t1 = time.time()
    ans = x*x/(x+x)
    t2 = time.time()
    id = os.getpid()
    print("In process {} ({:2d}) ans is {:8.4f} input is :{} time is {}".format(id,jobnum,ans,x,t2-t1))
    return (jobnum,t2-t1,id)

def test2(data,np):
    pool = Pool(processes=np)              
    # This is non-blocking
    async_results = pool.map_async(func=worker,iterable=data)
    # This call blocks until all results are available
    results = async_results.get()   
    print_pool_results(results,np)

n_processes = 4
n_jobs = 20

vals = [5*random.random() for i in range(n_jobs)]
data = zip(range(n_jobs),vals)    # Create list of tuples (p,vals)

if __name__ == '__main__':
    t1 = time.time()
    test2(data,n_processes)
    print("{:>25s} {:12.4f}".format("Wall clock time (s)",time.time()-t1))
