def mydecorator(f):

    def wrapper():
        print "Inside of the decorator before calling the function"
        f()
        print "Inside of the decorator after calling the function"

    return wrapper

@mydecorator
def printName():
    print "Ruthvik"


printName()
