import abc

class Rectangle(object):
    __metaclass__ = abc.ABCMeta

    def __init__(self, width, height):
        self.width = width
        self.height = height

    @abc.abstractmethod
    def area(self):
        return

class Square(Rectangle):
    def __init__(self, length):
        self. length = length
        #super(Square, self).__init__(length, length)
        Rectangle.__init__(self, length, length)
    def area(self):
        return self.length * self.length

s = Square(5)
