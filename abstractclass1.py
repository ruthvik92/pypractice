class AbstractClass():
    
    def do_something(self):
        pass

class B(AbstractClass):
    pass
a = AbstractClass()
b = B()
###In this code we are simply inheriting the class named AbstractClass into class named B.

## Rule1: Abstract classes may not be instantiated directly. In order to instantiate an 
    ## abstract class you need to implement the abstractmethods in the Abstract class definition.(This rule
    ## applies for inherited cases also. Always implement all the abstractmethods of the abstract class
    ## in the subclass.
