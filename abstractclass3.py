from abc import ABCMeta, abstractmethod
##for python 3+ see https://www.python-course.eu/python3_abstract_classes.php

class AbstractClass():
    
    __metaclass__ = ABCMeta
    
    def __init__(self,value):
        self.value = value

    @abstractmethod
    def do_something(self):
        pass


class DoAdd42(AbstractClass):
    def __init__(self,value):
        super(DoAdd42,self).__init__(value)

    def do_something(self):
        return self.value + 42

x = DoAdd42(4)
print(x.do_something())

### 
