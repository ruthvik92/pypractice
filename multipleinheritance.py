class Person(object):
    def __init__(self, name):
        self.name = name
        
    def getName(self):
        return self.name
    def isEmployee(self):
        return False

class Employee(Person):
    def __init__(self, salary,name):
        Person.__init__(self,name)
        self.salary = salary

    def isEmployee(self):
        return True


class CEO(Person, Employee):
    def __init__(self, name, salary, access):
        Employee.__init__(self, salary, name)
        self.access = access
        
    def isEmployee(self):
        print "yes, but he's the CEO"
    
    def getName(self):
        return self.name


emp = Person("Geek1")
print(emp.getName(), emp.isEmployee())

emp1 = Employee(1000,'geek') 
print(emp1.getName(), emp1.isEmployee())

print issubclass(Employee, Person)

print isinstance(emp1,Person)

emp2 = CEO('geek3', 1000, 'yes')
emp2.isEmployee()
print emp2.getName()


Employee.__mro__
