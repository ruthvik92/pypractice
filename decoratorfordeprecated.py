import warnings
def record(number):
    print "recorded",number

    
class deprecated(object):
    """
    Decorator to mark functions/methods as deprecated. Emits a warning when
    function is called and suggests a replacement.
    """

    def __init__(self, replacement=''):
        self.replacement = replacement

    def __call__(self, func):
        def new_func(*args, **kwargs):
            msg = "%s() is deprecated, and will be removed in a future release." % func.__name__
            if self.replacement:
                msg += " Use %s instead." % self.replacement
            warnings.warn(msg, category=DeprecationWarning)
            return func(*args, **kwargs)
        #new_func.__name__ = func.__name__
        #print new_func.__name__
        new_func.__doc__ = "*Deprecated*. Use ``%s`` instead." % self.replacement
        #new_func.__dict__.update(func.__dict__)
        #print new_func.__dict__
        return new_func



@deprecated("record('v')")
def record_v(number):
    
    """
    Record the membrane potential for all cells in the Population.
    """
    record(number)



record_v(2)
