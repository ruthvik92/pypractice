from abc import ABCMeta, abstractmethod

class AbstractClass():
    
    __metaclass__ = ABCMeta
    
    def __init__(self,value):
        self.value = value

    @abstractmethod
    def do_something(self):
        pass


class DoAdd42(AbstractClass):
    pass

x = DoAdd42()
##TypeError: Can't instantiate abstract class DoAdd42 with abstract methods do_something
